package com.ctt.demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 数据校验类
 */
public class Valdate {
    public static void main(String[] args) {


//        Set<String> newData = getFileDataList(App2.localFilePrefix+"5级行政区划数据/village 2.txt");
//        System.out.println("size:"+newData.size());


        List<String> errorPathList = new ArrayList<String>();//获取旧的错误列表文件
        File file = new File(App2.localFilePrefix+"village.txt");
        Set<String> s = new HashSet<String>();
        if(file.isFile() && file.exists()){
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuffer sb = new StringBuffer();
                String text = null;
                int count = 0;
                while((text = bufferedReader.readLine()) != null){
                    if(!"".equals(text)){
                        if(!s.contains(text)){
//                            System.out.println(text);
                            count ++;
                        }
                        s.add(text);
                    }
                }
                System.out.println("count:"+count);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    public static Set<String> getFileDataList(String filePath){
        Set<String> dataList = new HashSet<String>();
        File file = new File(filePath);
        if(file.isFile() && file.exists()){
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuffer sb = new StringBuffer();
                String text = null;
                int count = 0;
                while((text = bufferedReader.readLine()) != null){
                    if(!"".equals(text)){
                        if(dataList.contains(text)){
//                            System.out.println(text);
                            count ++;
                        }
                        dataList.add(text);
                    }
                }
                System.out.println(count);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return dataList;
    }

}
